# Exercice Symfony

## Introduction

Projet sous Symfony 5.

## Commandes

### Pré-requis

- Avoir **Composer** (<https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx>)
<!-- - Avoir **NodeJS** (<https://nodejs.org/en/download/package-manager/>) -->

### Initialisation du projet

Commencer par créer le fichier `.env.local`

```env
###> doctrine/doctrine-bundle ###
DATABASE_URL=mysql://<user>:<password>@127.0.0.1:3306/<db-name>
###< doctrine/doctrine-bundle ###
```

Puis, exécuter les commandes suivantes.

```bash
composer install
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
php bin/console doctrine:fixture:load
```

## L'exercice

### [] Créer l'entité `Book`

- [] Créer l'entité `Book`
  - doit contenir, au minimum, les attributs (hors relations) suivants
    - name
    - publicationDate
- [] Créer une relation entre `Book` et `Author`
- [] Créer une relation entre `Book` et `Genre`
- [] Modifier la fixture pour ajouter des instances de `Book`

Liens utiles

- Pour générer les entités et les controlleurs : [MakerBundle (Symfony)](https://symfony.com/doc/current/bundles/SymfonyMakerBundle/index.html)

### [] Créer le Crud Controller pour l'entité `Book` (EasyAdmin)

- [] Créer le crud Controller
  - [] Afficher la relation avec `Author`
  - [] Afficher la relation avec `Genre`
  - [] Dans la liste, ajouter les filtres sur les attributs suivant
    - name
    - publicationDate
    - relation avec `Author`
    - relation avec `Genre`
- [] Modifer AuthorCrudController
  - [] Afficher la relation avec `Book`
- [] Modifier GenreCrudController
  - [] Afficher la relation avec `Book`

Liens utiles

- [Documentation EasyAdmin](https://symfony.com/bundles/EasyAdminBundle/current/index.html)

### [] Créer une API Rest (API Platform)

- [] Créer, en Symfony, une API Rest qui permet de lister, consulter, ajouter, modifier et de supprimer un auteur, un livre ou un lecteur.
- [] Quand on liste ou consulte un `Author`, ajouter l'âge
  - Si décédé, calculer l'âge à sa mort
- [] Lors de la récupération d'un `Author`, i

Liens utiles

- [Documentation Api Platform](https://api-platform.com/docs)

## Informations complémentaires

- EasyAdmin et API Platform sont déjà installés
- Vous pouvez démarrer le serveur à l'aide de la commande suivante : `php -S localhost:8080 -t public/`
- Pour tester les appels API, vous pouvez aller sur ce lien : [http://127.0.0.1:8080/api/docs](http://127.0.0.1:8080/api/docs)
- Utilisez **git** pour versionnez votre exercice.
- Envoyer un email à l'adresse suivante : <contact@we-labz.com>. L'email doit contenir le lien vers votre projet sur **GitLab**.
